### 注意事项

- 统一引入 common.css
-

### 页面

- login.html -- 登录页
- regist.html -- 注册页
- inputed_regist.html -- 填写过内容的注册页
- find_back.html -- 找回密码页 done
- home.html -- 首页 done
- order.html -- 订单 done
- mine.html -- 我的 done
- add_address.html -- 添加地址 done
- address_list.html -- 地址列表 done
- add_shopping_cart.html -- 加入物车 done
- shopping_cart.html -- 购物车 done
- submit_order.html -- 订单 done
- change_phone.html -- 修改手机号
